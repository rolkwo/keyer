/*
 * main.cpp
 *
 *  Created on: May 29, 2017
 *      Author: roland
 */
#include <Arduino.h>

#include <Morse.h>

//#define DEBUG

#define PIN_KEY 12
#define PIN_PTT 13
#define DEFAULT_DIT_TIME 100

uint8_t ditTime = DEFAULT_DIT_TIME;

void interpretCommand();

enum Commands {DIT_TIME = 0};

Morse morse(PIN_KEY, DEFAULT_DIT_TIME);

void setup()
{
	Serial.begin(9600);
	Serial.println("Keyer started");

	pinMode(PIN_PTT, OUTPUT);
}

void loop()
{
	static uint32_t lastCharTime;
	if(Serial.available())
	{
		char c[1];
		c[0] = Serial.read();

		if(c[0] == 0)
		{
			interpretCommand();

			return;
		}

		digitalWrite(PIN_PTT, HIGH);
		delay(100);
		morse.sendString(c);
		Serial.print(c[0]);
		lastCharTime = millis();
	}
	else
	{
		if(millis() - lastCharTime >= 5 * ditTime)
		{
			digitalWrite(PIN_PTT, LOW);
		}
	}
}

void interpretCommand()
{
#ifdef DEBUG
	Serial.println("Interpret command");
#endif
	while(!Serial.available())
		;

	char cmd = Serial.read();

	switch(cmd)
	{
		case DIT_TIME:
		{
			while(!Serial.available())
				;
			ditTime = Serial.read() * 10;
#ifdef DEBUG
	Serial.print("Setting dit time: ");
	Serial.println(ditTime, DEC);
#endif
			morse.setDiTime(ditTime);
		}
		break;

		default:
			return;
	}
}


